package chapitre2.exercice2;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Mark {

    private float value; // désigne la moyenne de la note pour la matière
    private int coef;
    private String subject; // désigne le nom de la matière

    //Constructeur vide
    public Mark() {
        this.value = 0;
        this.subject = null;
    }

    //Constructeur avec paramètres
    public Mark(String subject,float value) {
        if (value>=0 && value<=20)
        this.value = value;
        this.subject = subject;
    }


    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        if (value>=0 && value<=20)
            this.value = value;
    }

    public String getSubject () {
        return subject;
    }
    public void setSubject (String subject) {
        this. subject = subject;
    }

    public String toString(){
        String str = super.toString() + "\n \t ==>>" + this.subject + ": "+ this.value;
        return str;
    }

    public Object clone()throws CloneNotSupportedException{
        Mark mark = null;

        mark = (Mark) super.clone();
        return mark;
    }

}
