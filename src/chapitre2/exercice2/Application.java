package chapitre2.exercice2;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Scanner;


public class Application {

    private HashSet<Mark> mark;
    private String nomEtudiant;
    private int cle;


    //Variables publiques qui comptent les instances
    public static int nbreInstances = 0;

    //Variables publiques qui comptent les instances
    public static int nbreInstancesBis = 0;

    //Constructeur par défaut
    public Application(){
        System.out.println("Création d'une application !");
        //On incrémente nos variables à chaque appel aux constructeurs
        nbreInstances++;
        nbreInstancesBis++;

        nomEtudiant = "Inconnu";
        cle = 0;
        mark = null;
    }

    //Constructeur 2
    Application(String nomEtudiant,HashSet<Mark> mark){
        this.cle = nbreInstances;
        this.nomEtudiant = nomEtudiant;
        this.mark = mark;
        nbreInstances++;
    }

    //*************   ACCESSEURS *************
    public String getnomEtudiant() {
        return nomEtudiant;
    }

    public int getCle()
    {
        return cle;
    }

    public HashSet getmark(){
        return mark;
    }
    //*************   MUTATEURS   *************
    public void setnomEtudiant(String pNom)
    {
        nomEtudiant = pNom;
    }

    public void setCle(int pCle)
    {
        cle = pCle;
    }

    public void setMark(HashSet pmark)
    {
        mark = pmark;
    }

    //*************   LES FONCTIONS/PROCEDURES   *************
    public boolean equals(Object obj) {
        //On vérifie si les références d'objets sont identiques
        if (this == obj)
            return true;

        //On s'assure que les objets sont du même type, ici de type Ville
        if (getClass() != obj.getClass())
            return false;

        //Maintenant, on compare les attributs de nos objets
        Application other = (Application) obj;

        return Objects.equals(other.getnomEtudiant(), this.getnomEtudiant()) &&
                Objects.equals(other.getCle(), this.getCle()) &&
                Objects.equals(other.getmark(), this.getmark());
    }


    /*public Object clone()throws CloneNotSupportedException{
        Application application = (Application) super.clone();
        application.mark = (HashSet)mark.clone();

        return application;
    }*/

    //Retourne la description d'un application
    public String print(){
        return "id:"+this.cle+" nom: "+this.nomEtudiant ;
    }

    public static float calculMoyenne(Application app, HashSet<Mark> set){

        float moy=0;
        float nbmatiere=0;
        for(Mark b:set){
            moy = b.getValue() + moy;
            nbmatiere+=1;
        }
       return  moy/nbmatiere;
    }

    public Mark creationMark(){

        Mark mark = new Mark();
        Scanner sc = new Scanner(System.in);
        mark.setSubject(sc.toString());
        mark.setValue(sc.nextInt());


        return mark;
    }


    public static void main(String args[]) {

        HashSet<Mark> matiere = new HashSet<Mark>();
        LinkedList<Application> etudiant = new LinkedList<Application>();

        //Creating Matiere d'un eleve
        Mark mark1 = new Mark("math",12);
        Mark mark2 = new Mark("informatique",10);
        Mark mark3 = new Mark("anglais",19);



        matiere.add(mark1);
        matiere.add(mark2);
        matiere.add(mark3);

        Application etudiant1 = new Application("Manitra", matiere);
        //Creation d'un etudiant
        etudiant.add(etudiant1);


        for(Application etu : etudiant) {
            System.out.print(etu.print());
            for (Mark b : matiere) {
                System.out.print("\t\n => " + b.getSubject() + " : " + b.getValue());
            }
        }


        float moyenne = calculMoyenne(etudiant1,matiere);

        System.out.println("\nmoyenne : " + moyenne);
    }

}
