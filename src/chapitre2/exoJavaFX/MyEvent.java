package chapitre2.exoJavaFX;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class MyEvent extends Application implements EventHandler<ActionEvent> {


    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Hello Moc");
        primaryStage.setWidth(400);
        primaryStage.setHeight(400);

        Button button = new Button("Clic");

        MyEvent myEvent = new MyEvent();
        button.addEventHandler(ActionEvent.ACTION, myEvent);
        Scene scene = new Scene(button);

        primaryStage.setScene(scene);
        primaryStage.show();



    }

    @Override
    public void handle(ActionEvent event) {
        System.out.println("Hello world");
    }

}
