package chapitre2.exoJavaFX;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class MyFirstView extends Application implements EventHandler<ActionEvent> {
    private int x = 180;
    private int y = 100;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Hello Moc");
        primaryStage.setWidth(400);
        primaryStage.setHeight(400);

        Pane pane = new Pane();
        Label label = new Label("texte");
        label.relocate(x, y);
        pane.getChildren().add(label);

        Button buttonLeft = new Button("gauche");
        buttonLeft.relocate(20, 300);

        Button buttonRight = new Button("droit");
        buttonRight.relocate(300, 300);

        buttonLeft.setOnAction(event -> {
            x -= 5;
            label.relocate(x, y);
        });
        buttonRight.setOnAction(event -> {
            x += 5;
            label.relocate(x, y);
        });


        pane.getChildren().add(buttonRight);
        pane.getChildren().add(buttonLeft);


        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    @Override
    public void handle(ActionEvent event) {

    }

}