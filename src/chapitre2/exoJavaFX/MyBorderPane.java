package chapitre2.exoJavaFX;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class MyBorderPane extends Application implements EventHandler<ActionEvent> {
    private int x = 180;
    private int y = 100;

    @Override
    public void start(Stage primaryStage) throws Exception {


        BorderPane root = new BorderPane();
        // TOP
        Button btnTop = new Button("Top");
        root.setTop(btnTop);

        // LEFT
        Button btnLeft = new Button("Left");
        root.setLeft(btnLeft);

        // CENTER
        Button btnCenter = new Button("Center");
        root.setCenter(btnCenter);
        BorderPane.setAlignment(btnCenter, Pos.BOTTOM_CENTER);

        // RIGHT
        Button btnRight = new Button("Right");
        root.setRight(btnRight);

        // B5
        Button btnBottom5 = new Button("B5");
        root.setBottom(btnBottom5);
        BorderPane.setAlignment(btnBottom5, Pos.TOP_CENTER);


        // B6
        Button btnBottom6 = new Button("B6");
        root.setBottom(btnBottom6);
        BorderPane.setAlignment(btnBottom6, Pos.TOP_RIGHT);


        Scene scene = new Scene(root, 550, 250);

        /*Button[] array = new Button[6];
        for(int i = 0; i < 6; i++) {
            array[i] = new Button("b" + (i+1));
            array[i].setMaxWidth(Double.MAX_VALUE);
            array[i].setMaxHeight(Double.MAX_VALUE);
            int finalI = i;
            array[i].setOnAction(event -> { System.out.println(array[finalI].getText()); });
        }
        */

        primaryStage.setTitle("BorderPane Layout Demo");
        primaryStage.setScene(scene);
        primaryStage.show();

    }


    @Override
    public void handle(ActionEvent event) {

    }
}