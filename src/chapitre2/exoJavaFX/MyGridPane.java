package chapitre2.exoJavaFX;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MyGridPane extends Application implements EventHandler<ActionEvent> {

    @Override
    public void start(Stage primaryStage) throws Exception {


        GridPane gp = new GridPane();
        Button[] array = new Button[6];
        for (int i = 0; i < 6; i++) {
            array[i] = new Button("b" + (i + 1));
            array[i].setMaxWidth(Double.MAX_VALUE);
            array[i].setMaxHeight(Double.MAX_VALUE);
            int finalI = i;
            array[i].setOnAction(event -> {
                System.out.println(array[finalI].getText());
            });
        }

        gp.add(array[0], 0, 0, 2, 1);
        gp.add(array[1], 2, 0, 1, 3);
        gp.add(array[2], 0, 1, 1, 1);
        gp.add(array[3], 1, 1, 1, 1);
        gp.add(array[4], 3, 1, 1, 2);
        gp.add(array[5], 4, 2, 1, 1);
        gp.setGridLinesVisible(false);
        Scene scene = new Scene(gp);

        primaryStage.setTitle("MyGridPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    @Override
    public void handle(ActionEvent event) {

    }
}
