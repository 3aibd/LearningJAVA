package chapitre2.JavaIO;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.io.RandomAccessFile;


public class Test2 {
    public static void deleteDouble(int pos, String fileName, Double remove) {
        ArrayList<Double> arrayList = new ArrayList<>();
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader file = new BufferedReader(fileReader);


        String line;
        try {
            while ((line = file.readLine()) != null) {
                line.split(" ");
                for (String value : line.split(" ")) {
                    System.out.println(value);
                    arrayList.add(Double.valueOf(value));
                }
                System.out.print(arrayList.get(pos));
                //System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try (PrintWriter pw = new PrintWriter(fileName)) {
            for (Double d : arrayList) {
                pw.println(d);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        try {
            RandomAccessFile file = new RandomAccessFile("test.txt", "rw");
            long position = file.length();
            while (position > 0) {
                position -= 1;
                file.seek(position);
                byte b = file.readByte();
                System.out.print((char) b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


