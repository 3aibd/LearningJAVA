package chapitre2.JavaIO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.RandomAccess;

public class correctionPierre {
    static void deleteDouble(int pos, String fileName){
        try{
            RandomAccessFile raf = new RandomAccessFile(fileName, "rw");
            long size = raf.length();
            if(pos < 0 || pos >size/8){
                return;
            }
            raf.seek(size-8);
            double lastValue = raf.readDouble();
            raf.seek(8*(pos>0 ? pos-1 : pos));
            raf.writeDouble(lastValue);
            raf.setLength(size-1);
            raf.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        deleteDouble(0,"test.txt");

    }
}
