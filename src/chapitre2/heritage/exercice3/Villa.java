package chapitre2.heritage.exercice3;

public class Villa extends Building {
    private int rCount;
    private boolean swimmingPool;

    public Villa(int rCount, boolean swimmingPool, String owner, String address, double surface) {
        super(owner, address, surface);
        this.rCount = rCount;
        this.swimmingPool = swimmingPool;
    }

    public String toString() {
        return "Villa [nbpieces=" + rCount + ", piscine=" + swimmingPool
                + ", toString()= " + super.toString() + "]";
    }

    public Villa() {
        super();
    }

    public int getRCount() {
        return rCount;
    }

    public void setRCount(int rCount) {
        this.rCount = rCount;
    }

    public boolean isSwimmingPool() {
        return swimmingPool;
    }

    public void setSwimmingPool(boolean swimmingPool) {
        this.swimmingPool = swimmingPool;
    }

    public double tax() {
        //L'impôt vaut 100€ par pièces + 750€ si jamais il y a une piscine.
        return 100 * rCount + (swimmingPool ? 750 : 0);
    }
}
