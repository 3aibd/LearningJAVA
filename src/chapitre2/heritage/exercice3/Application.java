package chapitre2.heritage.exercice3;

import java.util.Arrays;
import java.util.Comparator;

public class Application {

    public static void main(String[] args) {

        Building[] array = new Building[3];

        array[0] = new Villa(8, false, "Durand", "paris", 125);
        array[1] = new Company("Augustine SA", 10, 24, "Cunegonde", "paris", 125);
        array[2] = new Villa(6, true, "Albert", "lyon", 105);

        int i;
        int swim = 0;
        double taotalArea = 0.;
        double totalTax = 0.;

        for (i = 0; i < array.length; i++) {
            taotalArea += array[i].getSurface();
            totalTax += array[i].tax();
            if (array[i] instanceof Villa && ((Villa) array[i]).isSwimmingPool())
                swim++;
        }
        System.out.println("La surface totale est de " + taotalArea);
        System.out.println("L'impot total est de " + totalTax);
        System.out.println("Le nombre de piscines est de " + swim);

        for (i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        Arrays.sort(array);

        for (i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        Comparator<Building> compt = new Comparator<Building>() {
            @Override
            public int compare(Building o1, Building o2) {
                String ownerNameA = o1.getOwner();
                String ownerNameB = o2.getOwner();
                return ownerNameA.compareTo(ownerNameB);
            }
        };
        Arrays.sort(array, compt);

        for (i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
