package chapitre2.heritage.exercice4;

abstract public class Employee {
    Person person;
    float salary;
    int service;
    protected Employee next;
    private static Employee salaryList = null;


    public Employee(float sl, int sv) {
        this(null, sl, sv);
    }

    public Employee(String name, float salary, int service) {
        this.person = new Person(name,salary);
        this.salary = salary;
        this.service = service;
        next = salaryList;
        salaryList = this;
    }


    public static void printAll() {
        Employee inter = salaryList;
        while (inter != null) {
            inter.print();
            inter = inter.next;
        }
    }

    public void deleteAll() {
        Employee inter = salaryList, g;
        while (inter != null) {
            g = inter.next;
            inter.next = null;
            inter = g;
        }
        salaryList = null;
    }

    public void remove() {
        Employee inter = salaryList;
        if (inter == this) {
            salaryList = inter.next;
        } else {
            while (inter.next != this)
                inter = inter.next;
            inter.next = inter.next.next;
        }
    }

    abstract void print();
}

