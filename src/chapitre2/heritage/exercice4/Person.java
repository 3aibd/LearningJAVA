package chapitre2.heritage.exercice4;

public class Person {
    protected String name;
    protected double salaire;

    public Person() {
    }

    public Person(String name,double salaire) {
        this.name = name;
        this.salaire = salaire;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    public String toString() {
        return name + salaire;
    }

    public void print() {
        System.out.println(name + salaire);
    }


}
