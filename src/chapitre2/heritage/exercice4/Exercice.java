package chapitre2.heritage.exercice4;

public class Exercice {
    public static void main(String args[]) {
        Worker r1 = new Worker ("Mathieu",1500,1,169);
        Worker r2 = new Worker ("Valentin",1300,2,182);
        Worker r3 = new Worker ("Vincent",1200,3,167);
        Employee.printAll();
        r3.remove();
        Employee.printAll();
        r3.deleteAll();
        Employee.printAll();
    }
}
