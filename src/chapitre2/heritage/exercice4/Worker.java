package chapitre2.heritage.exercice4;

public class Worker extends Employee {
    int hours;

    public Worker(String name, float salary, int service) {
        super(name, salary, service);

    }

    public Worker(String name, float salary, int service, int hours) {
        super(name, salary, service);
        this.hours = hours;
    }

    public void print() {
        System.out.println("service :"+ Worker.this.service + "\t nom : " + person.name + " \t salaire : " + person.getSalaire() + "\t hours: "+ hours);
    }

}
