package chapitre2.heritage.exercice2;

public final class Sphere extends Shape3D {
    private double radius = 0.0;

    /**
     * Crée une sphere
     *
     * @param name   le nom de la sphere
     * @param radius le rayon de la sphere
     */
    public Sphere(String name, double radius) {
        super(name);
        this.radius = radius;
    }

    /**
     * Calcule le volume de la sphere * @return le volume de la sphere
     */
    public double volume() {
        return (4. / 3.) * Math.PI * radius * radius * radius;
    }

    /**
     * Calcule l'aire de la sphere * @return l'aire de la sphere
     */
    public double surface() {
        return 4 * Math.PI * radius * radius;
    }
}