package chapitre2.heritage.exercice2;

/**
 * Classe abstraite, base de toutes les figures 3D
 */
public abstract class Shape3D extends Shape {
    public static int counter = 0;

    Shape3D(String name) {
        super(name);
        counter++;
    }

    protected void finalize() {
        System.out.println("Shape3D.finalize");
        counter--;
        super.finalize();
    }

    /**
     * Calcule le volume de la figure * @return le volume de la figure
     */
    public abstract double volume();
}