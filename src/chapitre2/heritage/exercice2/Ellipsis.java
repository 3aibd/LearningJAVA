package chapitre2.heritage.exercice2;

/**
 * Cette classe gère des ellipses
 */
public class Ellipsis extends Shape2D {
    private double rayA;
    private double rayB;

    /**
     * Crée une ellipse
     *
     * @param name le nom de l'ellipse
     * @param rayA le rayon horizontal de l'ellipse
     * @param rayB le rayon vertical de l'ellipse
     */
    public Ellipsis(String name, double rayA, double rayB) {
        super(name);
        this.rayA = rayA;
        this.rayB = rayB;
    }

    /**
     * Calcule l'aire de l'ellipse * @return l'aire de l'ellipse
     */
    public double surface() {
        return Math.PI * rayA * rayB;
    }

    /**
     * calcule le perimetre de l'ellipse * @return le perimetre de l'ellipse
     */
    public double perimeter() {
        return 2 * Math.PI * Math.sqrt(0.5 * (rayA * rayA + rayB * rayB));
    }

    public double getRayA() {
        return rayA;
    }

    public void setRayA(double rayA) {
        this.rayA = rayA;
    }

    public double getRayB() {
        return rayB;
    }

    public void setRayB(double rayB) {
        this.rayB = rayB;
    }
}