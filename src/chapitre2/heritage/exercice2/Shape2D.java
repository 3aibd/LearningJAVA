package chapitre2.heritage.exercice2;

/**
 * Classe abstraite, base de toutes les figures 2D
 */
public abstract class Shape2D extends Shape {
    public static int counter = 0;

    public Shape2D(String nom) {
        super(nom);
        counter++;
    }

    protected void finalize() {
        System.out.println("Forme2D.finalize");
        counter--;
        super.finalize();
    }

    /**
     * Calcule le perimetre de la figure * @return le perimetre de la figure
     */
    public abstract double perimeter();
}
