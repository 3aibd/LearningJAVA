package chapitre2.heritage.exercice2;

/**
 * Cette classe gère des carrés
 */
public final class Square extends Rectangle {
    /**
     * Crée un carre
     *
     * @param name le nom du carré
     * @param side la longueur du coté du carré
     */
    public Square(String name, double side) {
        super(name, side, side);
    }
}