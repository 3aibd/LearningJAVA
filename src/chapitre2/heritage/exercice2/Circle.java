package chapitre2.heritage.exercice2;

/**
 * Cette classe gère des cercles
 */
public final class Circle extends Ellipsis {
    /**
     * Crée un cercle
     *
     * @param name le nom du cercle
     * @param r    le rayon du cercle
     */
    public Circle(String name, double r) {
        super(name, r, r);
    }

}