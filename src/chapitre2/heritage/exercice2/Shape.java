package chapitre2.heritage.exercice2;

public abstract class Shape {
    public static int counter = 0;
    private String name;

    public Shape(String name) {
        counter++;
        this.name = name;
    }

    /**
     * Appelée à chaque désallocation d'un objet
     */
    protected void finalize() {
        System.out.println("Shape.finalize");
        counter--;
    }

    /**
     * Calcule l'aire de la figure
     *
     * @return l'aire de la figure
     */
    public abstract double surface();

    /**
     * accesseur en lecture
     *
     * @return le nom
     */
    public String getName() {
        return name;
    }

    /**
     * accesseur en écriture
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
}