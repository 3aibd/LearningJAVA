package chapitre2.heritage.exercice2;



/**
 * Cette classe gere des cylindres
 */
public final class Cylinder extends Shape3D {
    private double radius = 0.0;
    private double height = 0.0;

    /**
     * Crée un cylindre
     *
     * @param name   le nom du cylindre
     * @param radius le rayon du cylindre
     * @param height la hauteur du cylindre
     */
    public Cylinder(String name, double radius, double height) {
        super(name);
        this.radius = radius;
        this.height = height;
    }

    /**
     * Calcule le volume du cylindre * @return le volume du cylindre
     */
    public double volume() {
        return Math.PI * radius * radius * height;
    }

    /**
     * calculel'aire du cylindre
     *
     * @return l'aire du cylindre
     */
    public double surface() {
        return (height + 2) * (2 * Math.PI * radius);
    }

    public double getHeight() {
        return height;
    }

    public void setheight(double h) {
        this.height = h;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double r) {
        this.radius = r;
    }
}