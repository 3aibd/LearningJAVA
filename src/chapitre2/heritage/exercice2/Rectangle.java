package chapitre2.heritage.exercice2;

/**
 * Cette classe gère des rectangles
 */
public class Rectangle extends Shape2D {
    private double width;
    private double height;

    /**
     * Crée un rectangle
     *
     * @param name   le nom du rectangle
     * @param width  la largeur du rectangle
     * @param height la hauteur du rectangle
     */
    public Rectangle(String name, double width, double height) {
        super(name);
        this.width = width;
        this.height = height;
    }

    /**
     * calcule l'aire du rectangle * @return l'aire du rectangle
     */
    public double surface() {
        return width * height;
    }

    /**
     * calcule le perimetre du rectangle * @return le perimetre du rectangle
     */
    public double perimeter() {
        return 2 * (width + height);
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}