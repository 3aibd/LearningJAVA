package chapitre2.heritage.exercice5;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class Application {
    void print(String className) throws ClassNotFoundException {
        Class c = Class.forName(className);
        System.out.println("pour " + this.getClass().getName());
        Field[] fields = c.getDeclaredFields();
        for (Field field : fields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                System.out.println(field.getName() + " : " + annotation.annotationType() + ", " + ((Notice) annotation).value());
            }
        }
    }

    void print(String className, int compValue) throws ClassNotFoundException {
        Class c = Class.forName(className);
        System.out.println("pour " + this.getClass().getName());
        Field[] fields = c.getDeclaredFields();
        for (Field field : fields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                int value = ((Notice) annotation).value();
                if (value >= compValue)
                    System.out.println(field.getName() + " : " + annotation.annotationType() + ", " +
                            ((Notice) annotation).value());
            }
        }
    }

    void deepNoticePrint(Class<?> c) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        System.out.println("pour " + c.getName());
        Field[] fields = c.getDeclaredFields();
        for (Field field : fields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof NoticePrint) {
                    boolean value = ((NoticePrint) annotation).value();
                    if (value && (field.getType().isPrimitive() ||
                            field.getType().getName().equals("java.lang.String"))) {
                        System.out.println(field.getName() + " : " + annotation.annotationType() + ", " +
                                ((NoticePrint) annotation).value());
                    } else if (value && (!field.getType().isPrimitive())) {
                        deepNoticePrint(field.getType());
                    }
                }
            }
        }
    }

    public static void main(String args[]) throws ClassNotFoundException {
        Application application = new Application();
        application.print("chapitre2.heritage.exercice5.AnnotationUseExample");
    }
}
