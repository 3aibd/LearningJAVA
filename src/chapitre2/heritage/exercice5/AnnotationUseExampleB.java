package chapitre2.heritage.exercice5;

public class AnnotationUseExampleB {
    @NoticePrint(value = true)
    private int field1;
    private float field2;
    @NoticePrint
    public String field3;
    @NoticePrint(value = true)
    public String field4;
}
