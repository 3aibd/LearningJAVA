package chapitre2.heritage.exercice5;

public class AnnotationUseExample {
    @Notice(value = 10)
    private int fieldA;
    private float fieldB;
    @Notice(value = 20)
    public String fieldC;
}
