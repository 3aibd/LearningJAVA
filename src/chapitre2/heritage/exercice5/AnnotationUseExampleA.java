package chapitre2.heritage.exercice5;

public class AnnotationUseExampleA {
    @NoticePrint(value = true)
    private int fieldA;
    private float fieldB;
    @NoticePrint(value = true)
    public String fieldC;
    @NoticePrint(value = true)
    public AnnotationUseExampleB fieldD;
}