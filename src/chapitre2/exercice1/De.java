package chapitre2.exercice1;
import java.util.*;

public class De {
    int val;
    public Integer nbAleatoire;


    //construtor
    De(int val){
        this.val=val;
    }


    //sether val
    public int getVal() {
        return val;
    }
    //sether val
    public void setVal(int val) {
        this.val = val;
    }


    //Fonction du nb lancé
    public static int nbLancer(){
        Scanner sc = new Scanner(System.in);
        System.out.println("combien de fois voulez vous lancer le dé ?");
        int nbLance = sc.nextInt();
      return nbLance;
    }

    //Fonction de lancement de De
    public Integer lancerDe(De de){
        Random nbRandom = new Random();
        nbAleatoire = 1 + nbRandom.nextInt(7 - 1);
        de.setVal(nbAleatoire);
        return de.val;
    }


    public static void main(String args[]){

        int val=0;
        boolean game = false;

        //Demande de nb lancé
        int lancement = nbLancer();
        De d1=new De(val);
        De d2=new De(val);

        //intération de lancé
        for(int i=1; i<=lancement ; i++){

            int valueDe1 = d1.lancerDe(d1);
            int valueDe2 = d2.lancerDe(d2);

            System.out.println("lancé "+i +" sur de1"+":"+ valueDe1);
            System.out.println("lancé "+i +" sur de2"+":"+ valueDe2);

            if(valueDe1==valueDe2){
                System.out.println("Vous avez Gagné");
                game=true;
            }
        }
        if(!game){
            System.out.println("Vous avez perdu");
        }

    }
}
