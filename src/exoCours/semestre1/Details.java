package exoCours.semestre1;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;

public class Details {

    public static void main(String args[]) {

        /* This is how to declare HashMap */
        HashMap<Integer, String> hmap = new HashMap<Integer, String>();
        HashMap<String, Integer> map = new HashMap<>() ;

        String str = " bon jour bon jour mon";
        String[] Spliter = str.split(" ");

        /*Adding elements to HashMap*/
        //hmap.put(2, "Bon");
        //hmap.put(2, "jour");
        //hmap.put(-1, "mon");

        map.put("bon", 2);
        map.put("jour", 2);
        map.put("mon", -1);

        /* Display content using Iterator*/
        //Set set = hmap.entrySet();
        //Iterator iterator = set.iterator();

        Set set = map.entrySet();
        Iterator iterator = set.iterator();

        /*while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            System.out.print("key is: "+ mentry.getKey() + " & Value is: ");
            System.out.println(mentry.getValue());
        }
        */

        for(String key : map.keySet()){  // convertir en string pr récupérer la valeur
            System.out.println("clé :"   + key) ;
            System.out.println("valeur :" + map.get(key)) ;
        }

        for(String key : Spliter){  // convertir en string pr récupérer la valeur
            if(map.containsKey(key)){
                //map.put(key, map.get(key)>1);
            }
            System.out.println("clé :"   + key) ;
            System.out.println("valeur :" + map.get(key)) ;
        }

    }
}
