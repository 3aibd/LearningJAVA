package exoCours.semestre1;

import java.util.Arrays;
import java.util.List;

public class TestStringBuilding {

    private static final int NB_CONCAT = 100;
    private static final int NB_RUN = 1000;

    private static List<String> TOKENS = Arrays.asList("zero", "one", "thow", "three", "four", "five", "six", "seven", "eight", "nine");

    public static void main(String[] args){
        long start = System.currentTimeMillis();

        for(int loop = 0 ; loop < NB_RUN; loop++){
            String text = "";
            for(int i = 0; i < NB_CONCAT; i++){
                text += TOKENS.get(i % 10);
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("String concatenation: "+(end-start)+" ms");

        start = System.currentTimeMillis();
        for(int loop = 0 ; loop < NB_RUN; loop++){
            StringBuffer text = new StringBuffer();
            for(int i = 0; i < NB_CONCAT; i++){
                text.append(TOKENS.get(i % 10));
            }
        }
        end = System.currentTimeMillis();
        System.out.println("StringBuffer append: "+(end-start)+" ms");

        start = System.currentTimeMillis();
        for(int loop = 0 ; loop < NB_RUN; loop++){
            StringBuilder text = new StringBuilder();
            for(int i = 0; i < NB_CONCAT; i++){
                text.append(TOKENS.get(i % 10));
            }
        }
        end = System.currentTimeMillis();
        System.out.println("StringBuilder append: "+(end-start)+" ms");
    }

}
