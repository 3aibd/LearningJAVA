package exoCours.semestre1;

public class Student implements Cloneable{
    double moyen;
    String name;
   private Address address;



    Student(double moyen,String name,Address adress){
        this.moyen=moyen;
        this.name=name;
        this.address=adress;

    }

    public Object clone()throws CloneNotSupportedException{
        Student student = (Student)super.clone();
        student.address = (Address)address.clone();

        return student;
    }


    /*
    public static void main(String args[]){
        try{
            Student s1=new Student(10,"manitra","toto");

            Student s2=(Student)s1.clone();

            System.out.println(s1.moyen+" "+s1.name);
            System.out.println(s2.moyen+" "+s2.name);

        }catch(CloneNotSupportedException c){}

    }
    */
}
