package exoCours.semestre1;

public class printTree {

    private Class classe;

    private String nomClasse;


    public static void printTree(String className) throws ClassNotFoundException {
        Class<?> currentClass = Class.forName(className);
        while (currentClass != null) {
            System.out.println(currentClass.getName());
            currentClass = currentClass.getSuperclass();
        }
    }

    public static void main(String[] args) throws ClassNotFoundException {

    }

}
