package exoCours.semestre2.heritage;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args){
        //avec les classes anonymes
        Compute d = new Compute() {
            public double calcul(double a,double b) {
                System.out.println(a + ";" + b);
                return a+b;
            }
        };
        d.calcul(1,3);

        //Avec la lambdas
        //Compute d2 = (s) -> System.out.println(s+";");
        //d2.calcul(3,5);

        List<Integer> myList = new ArrayList<Integer>();
        myList.add(5);
        myList.add(3);
        myList.add(9);
        myList.add(2);

        myList.forEach((value)->{System.out.print(value);});
    }

}
