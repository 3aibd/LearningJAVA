package exoCours.semestre2.heritage;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static java.lang.Class.forName;

public class Student {
    private double age;
    private String name;

    public Object clone()throws CloneNotSupportedException{
        Student student = (Student)super.clone();

        return student;
    }

    public static void printTree(String className) throws ClassNotFoundException {
        Class<?> currentClass = forName(className);
        while (currentClass != null){
            System.out.println(currentClass.getName());
            currentClass = currentClass.getSuperclass();
        }
    }

    public static void test(){
        System.out.println("test");
    }


    public static boolean methodExist(String methodName){
        Method[] methodses = Student.class.getClass().getMethods();
        for (Method method : methodses) {
            System.out.println(method.getName());
            if(methodName.equals(method.getName())){
                return true;
            }
        }
        return false;
    }

    public static Object instantiate(String className, String value) throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Class<?> c = Class.forName(className);
        Constructor construct = c.getConstructor(String.class);
        if(construct==null) return null;

        Object o = construct.newInstance(value);
        return 0;
    }

    public static void main(String[] args) {
        boolean result = methodExist("toString");
        if(result == true){
            System.out.println("method présente");
        } else {
            System.out.println("method non présente");
        }
    }
}