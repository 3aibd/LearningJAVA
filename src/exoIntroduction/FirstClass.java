package exoIntroduction;

import java.lang.String;

public class FirstClass {

    public static  void affichageTableau(int array[][]){
        //Methode 1
        for(int[]i: array){
            for(int value:i){
                System.out.println(value);
            }
        }
        //Méthode 2
        for(int i=0; i<array.length;i++){
            for(int j=0; j<array[i].length;j++) {
                System.out.println(array[i][j]);
            }

        }
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        /*int i;
        int max;
        Scanner sc = new Scanner(System.in);
        max = sc.nextInt();


        for(i=0; i<500; i++) {
            System.out.println("Nb" + i);
        }
        */
        //Exercice 1
        /*String mot="mot";
        String mot2 = mot.substring(0,1).toUpperCase() + mot.substring(1);
        System.out.println(mot2);

        //Exercice 2
        String mail = "adr@mail.fr";
        //int pt = mail.indexOf(".");
        //int at = mail.indexOf("@");

        String partA;
        String partB;
        String partC;

        partA=mail.substring(0,mail.indexOf("@")).toUpperCase();
        partB=mail.substring(mail.indexOf("@")+1, mail.indexOf(".")).toUpperCase();
        partC=mail.substring(mail.indexOf(".")+1, mail.length()).toUpperCase();
        System.out.println(partA + "\n" + partB + "\n" + partC);

        */
        int []arrayA = {10,20,30};
        int []arrayB = {30,40,50};
        int tmp;

        //Methode 1
        for(int i=0; i<arrayA.length;i++) {
            tmp = arrayA[i];
            arrayA[i] = arrayB[i];
            arrayB[i] = tmp;
        }

        for(int i=0; i<arrayA.length;i++){
            System.out.println(arrayA[i]);
        }

        //Methode 2 mais marche pas dans une fonction


    }

}

/*
MES NOTES :
Concaténation : .append("");
ArrayList<String> array;
array = new Array<String>();
//  Ajout :
    array.add("abc");
    get(0);
    size();

 */
/*
 * Exercice 1 :
 EXEMPLE Afficher des paires entre 1 et 500
 Test compil : javac firstpackage.FirstClass.java
 java firstpackage.FirstClass

 Exemple : Afficher entre 1 et une borne saisi


 * Exercice 2 :
 * entrée : mot
 * sortie : MOT
 *
 * entrée : adr@mail.fr
 *
 * -> ADR
 * -> MAIL
 * -> FR
 * Mettre en majuscule

*/
/*
exercice 1:
int[] arrayA = {10,20,30};
int[] arrayB = {30,40,50};

DONNER 2 Méthodes:


Exercice 2 :
    tableau de type :[][][][][]
                     []
    print
 */