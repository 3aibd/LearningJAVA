package chapitre1;
//Package à importer afin d'utiliser l'objet File
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.time.*;

//Package à importer afin d'utiliser l'objet Scanner


public class Exercice2 {

//-------------------------------------------------------------------------------
//Sous-programme
//-------------------------------------------------------------------------------

    //Sous programme qui recherche la date ou le mois pour renvoyer l indice du Array
    public  static int researchCA(ArrayList list, String dateDonnee){

        int indice = 0;

        for(int i = 0; i < list.size(); i++)
        {
            if(list.get(i).equals(dateDonnee)){
                System.out.println(list.get(i));
                indice = i ;
            }
        }

        return indice;
    }


    public  static int researchCAmounth(ArrayList list, String dateDonnee){

        int indice = 0;


        for(int i = 0; i < list.size(); i++)
        {
            if(list.get(i).equals(dateDonnee)){
                System.out.println(list.get(i));
                indice = i ;
            }
        }

        return indice;
    }

    //affiche un element du tableau
    public static void printTab(ArrayList myList,int indice){
        int emplacement = indice;
        System.out.println("maList" + "[" + emplacement + "]" + " = " + myList.get(emplacement));
        emplacement ++;
        System.out.println("maList" + "[" + emplacement+ "]" + " = " + myList.get(emplacement));
    }



//----------------------------------------------
//Programme principal du main
//----------------------------------------------
    public static void main(String[] args) {

        //Stockage du File
        File file = new File("texte");

        //Création d'un ArrayList
        ArrayList<String> maList = new ArrayList();

        //lecture du fichier
        Scanner scanner = null;

        //traitement de l'chap7exceptions
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //Déclaration des variables
        Scanner sc = new Scanner(System.in);
        double maSomme = 0;
        int cpt=0;
        int indice = 0;
        String dateDonnee;


        //Heure actuelle
        Instant start = Instant.now() ;

        //Constructeur
        



        //Boucle pour stocker les donnée du fichier dans un Array
        while(scanner.hasNextLine()){

            String value = scanner.nextLine();
            //System.out.println(value);

            maList.add(value.substring(0,value.indexOf(":")));
            maList.add(value.substring(value.indexOf(":") + 1,value.length()));

            System.out.println("maList" + "[" + cpt + "]" + " = " + maList.get(cpt));

            if(cpt%2 !=0){
                maSomme += Double.parseDouble(maList.get(cpt).toString());
            }

            cpt++;
        }


        //Affichage de la Somme
        System.out.println("Somme: " + maSomme);

        //Demande une date à l'utilisateur
        System.out.println("Donner une date : ");
        dateDonnee = sc.nextLine();

        indice = researchCA(maList,dateDonnee);
        System.out.println("l'indice : " + indice);
        printTab(maList,indice);


        //maList.forEach((a)->System.out.println());

        // calcul d'une durée
        Duration duration = Duration.between(start, Instant.now()) ;
        System.out.println(duration);

    }
}
