package openclassroom.Niv1.chap11flux.patternDecorator;

public abstract class Patisserie {
    public abstract String preparer();
}
