package openclassroom.Niv1.chap11flux.patternDecorator;

public class CoucheCaramel extends Couche{
    public CoucheCaramel(Patisserie p) {
        super(p);
        this.nom = "\t- Une couche de caramel.\n";
    }
}
