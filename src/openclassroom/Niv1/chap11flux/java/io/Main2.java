package openclassroom.Niv1.chap11flux.java.io;
//Packages à importer afin d'utiliser les objets
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main2 {

    public static void main(String[] args) {
        // Nous déclarons nos objets en dehors du bloc try/catch
        FileInputStream fis = null;
        FileOutputStream fos = null;

        try {
            // On instancie nos objets :
            // fis va lire le fichier
            // fos va écrire dans le nouveau !
            //File f = new File("openclassroom/chap11flux/test.txt");
            File f = new File("test");

            fis = new FileInputStream(new File(String.valueOf(f)));
            fos = new FileOutputStream(new File("test2"));

            // On crée un tableau de byte pour indiquer le nombre de bytes lus à
            // chaque tour de boucle
            byte[] buf = new byte[8];

            // On crée une variable de type int pour y affecter le résultat de
            // la lecture
            // Vaut -1 quand c'est fini
            int n = 0;

            // Tant que l'affectation dans la variable est possible, on boucle
            // Lorsque la lecture du fichier est terminée l'affectation n'est
            // plus possible !
            // On sort donc de la boucle
            while ((n = fis.read(buf)) >= 0) {
                // On écrit dans notre deuxième fichier avec l'objet adéquat
                fos.write(buf);
                // On affiche ce qu'a lu notre boucle au format byte et au
                // format char
                for (byte bit : buf) {
                    System.out.print("\t" + bit + "(" + (char) bit + ")");
                }
                System.out.println("");
                //Nous réinitialisons le buffer à vide
                //au cas où les derniers byte lus ne soient pas un multiple de 8
                //Ceci permet d'avoir un buffer vierge à chaque lecture et ne pas avoir de doublon en fin de fichier
                buf = new byte[8];

            }
            System.out.println("Copie terminée !");

        } catch (FileNotFoundException e) {
            // Cette chap7exceptions est levée si l'objet FileInputStream ne trouve
            // aucun fichier
            e.printStackTrace();
        } catch (IOException e) {
            // Celle-ci se produit lors d'une erreur d'écriture ou de lecture
            e.printStackTrace();
        } finally {
            // On ferme nos chap11flux de données dans un bloc finally pour s'assurer
            // que ces instructions seront exécutées dans tous les cas même si
            // une chap7exceptions est levée !
            try {
                if (fis != null)
                    fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (fos != null)
                    fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

/*

Java 7 initie ce qu'on appelle vulgairement le « try-with-resources ».
Ceci vous permet de déclarer les ressources utilisées
directement dans le bloctry(…),
ces dernières seront automatiquement fermées à la fin du bloc d'instructions !
Ainsi, si nous reprenons notre code de début de chapitre qui copie notre fichiertest.txtverstest2.txt,
nous aurons ceci :


try(FileInputStream fis = new FileInputStream("test.txt");
FileOutputStream fos = new FileOutputStream("test2.txt")) {
  byte[] buf = new byte[8];
  int n = 0;
  while((n = fis.read(buf)) >= 0){
    fos.write(buf);
    for(byte bit : buf)
      System.out.print("\t" + bit + "(" + (char)bit + ")");

    System.out.println("");
  }

  System.out.println("Copie terminée !");

} catch (IOException e) {
  e.printStackTrace();
}
 */

