package openclassroom.Niv1.chap11flux.java.nio2;
//Package à importer afin d'utiliser l'objet File
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;


public class MainNIO2 {

    public static void main(String[] args) {
        //Création de l'objet File
        File f = new File("test");
        System.out.println("Chemin absolu du fichier : " + f.getAbsolutePath());
        System.out.println("Nom du fichier : " + f.getName());
        System.out.println("Est-ce qu'il existe ? " + f.exists());
        System.out.println("Est-ce un répertoire ? " + f.isDirectory());
        System.out.println("Est-ce un fichier ? " + f.isFile());

        System.out.println("Affichage des lecteurs à la racine du PC : ");

        //On récupère maintenant la liste des répertoires dans une collection typée
        //Via l'objet FileSystem qui représente le système de fichier de l'OS hébergeant la JVM
        Iterable<Path> roots = FileSystems.getDefault().getRootDirectories();

        //Maintenant, il ne nous reste plus qu'à parcourir
        for (Path chemin : roots) {
            System.out.println(chemin);
            //Pour lister un répertoire, il faut utiliser l'objet DirectoryStream
            //L'objet Files permet de créer ce type d'objet afin de pouvoir l'utiliser
            try (DirectoryStream<Path> listing = Files.newDirectoryStream(chemin)) {

                int i = 0;
                for (Path nom : listing) {
                    System.out.print("\t\t" + ((Files.isDirectory(nom)) ? nom + "/" : nom));
                    i++;
                    if (i % 4 == 0) System.out.println("\n");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //La copie de fichier
        //Pour copier le fichiertest.txtvers un fichiertest2.txt, il suffit de faire :
        Path source = Paths.get("test");
        Path cible = Paths.get("test2.txt");
        try {
            Files.copy(source, cible, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) { e.printStackTrace();	}

        //Le déplacement de fichier
        //Pour déplacer le fichiertest2.txtvers un fichiertest3.txt, il suffit de faire :

        Path cible2 = Paths.get("test3.txt");
        try {
            Files.move(source, cible2, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) { e.printStackTrace();	}

        //Ouvrir des chap11flux
        /*
        //Ouverture en lecture :
        try ( InputStream input = Files.newInputStream(source) ) { } catch (IOException e) {
            e.printStackTrace();
        }

        //Ouverture en écriture :
        try ( OutputStream output = Files.newOutputStream(source) )  { } catch (IOException e) {
            e.printStackTrace();
        }

        //Ouverture d'un Reader en lecture :
        try ( BufferedReader reader = Files.newBufferedReader(source, StandardCharsets.UTF_8) )  { } catch (IOException e) {
            e.printStackTrace();
        }

        //Ouverture d'un Writer en écriture :
        try ( BufferedWriter writer = Files.newBufferedWriter(source, StandardCharsets.UTF_8) )  { } catch (IOException e) {
            e.printStackTrace();
        }
        */

        //GERER UN ZIP
        /*
        // Création d'un système de fichiers en fonction d'un fichier ZIP
        try (FileSystem zipFS = FileSystems.newFileSystem(Paths.get("monFichier.zip"), null)) {

            //Suppression d'un fichier à l'intérieur du ZIP :
            Files.deleteIfExists( zipFS.getPath("test.txt") );

            //Création d'un fichier à l'intérieur du ZIP :
            Path path = zipFS.getPath("nouveau.txt");
            String message = "Hello World !!!";
            Files.write(path, message.getBytes());

            //Parcours des éléments à l'intérieur du ZIP :
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(zipFS.getPath("/"))) {
                for (Path entry : stream) {
                    System.out.println(entry);
                }
            }

            //Copie d'un fichier du disque vers l'archive ZIP :
            Files.copy(Paths.get("fichierSurDisque.txt"), zipFS.getPath("fichierDansZIP.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

    }

}

//filtre du listing
//try(DirectoryStream<Path> listing = Files.newDirectoryStream(chemin, "*.txt")){ … }
//Ne prendra en compte que les fichier ayant l'extension .txt

/*
La copie de fichier

Le troisième argument permet de spécifier les options de copie. Voici celles qui sont disponibles :
StandardCopyOption.REPLACE_EXISTING: remplace le fichier cible même s'il existe déjà ;
StandardCopyOption.COPY_ATTRIBUTES: copie les attributs du fichier source sur le fichier cible (droits en lecture etc.) ;
StandardCopyOption.ATOMIC_MOVE: copie atomique ;
LinkOption.NOFOLLOW_LINKS: ne prendra pas en compte les liens.



Le déplacement de fichier

Dans le même genre vous avez aussi :
une méthodeFiles.delete(path)qui supprime un fichier ;
une méthodeFiles.createFile(path)qui permet de créer un fichier vide.


 */

/*
 gérer les fichier ZIP :
 est également possible d'être averti via l'objetWatchServicelorsqu'un un fichier est modifié, de gérer des entrées/sorties
 asynchrones via les objetsAsynchronousFileChannel,AsynchronousSocketChannelouAsynchronousServerSocketChannel.
 Ceci permet de faire les actions en tâche de fond, sans bloquer le code pendant l'exécution.
 Il est aussi possible d'avoir accès aux attributs grâce à 6 vues permettant de voir plus ou moins d'informations, à savoir :

BasicFileAttributeViewpermet un accès aux propriétés généralement communes à tous les systèmes de fichiers ;

DosFileAttributeViewajoute le support des attributs MS-DOS (readonly,hidden,system,archive) à l'objet ci-dessus ;

PosixFileAttributeViewajoute les permissions POSIX du monde Unix au premier objet cité ;

FileOwnerAttributeViewpermet de manipuler le propriétaire du fichier ;

AclFileAttributeViewpermet de manipuler les droits d'accès au fichier ;

UserDefinedFileAttributeView: permet de définir des attributs personnalisés.

 */