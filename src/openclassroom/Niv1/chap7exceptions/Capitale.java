package openclassroom.Niv1.chap7exceptions;

public class Capitale extends Ville {


    private String monument;

    //Constructeur par défaut
    public Capitale() {
        //Ce mot clé appelle le constructeur de la classe mère
        super();
        monument = "aucun";
    }
    //Constructeur d'initialisation de capitale
    public Capitale(String nom, int hab, String pays, String monument) throws NombreHabitantException, NomVilleException {
        super(nom, hab, pays);
        this.monument = monument;
    }

    /**
     * Description d'une capitale
     * @return String retourne la description de l'objet
     */
    public String decrisToi(){
        String str = super.decrisToi() + "\n \t ==>>" + this.monument + "en est un monument";

        return str;
    }

    public String toString(){
        String str = super.toString() + "\n \t ==>>" + this.monument + " en est un monument";
        return str;
    }




    /**
     * @return le nom du monument
     */
    //*************   ACCESSEURS *************
    public String getMonument() {
        return monument;
    }

    //*************   MUTATEURS   *************
    //Définit le nom du monument
    public void setMonument(String monument) {
        this.monument = monument;
    }


    //*******************************************************       MAIN       ************************************************
    public static void main(String[] args) {

    }
}

