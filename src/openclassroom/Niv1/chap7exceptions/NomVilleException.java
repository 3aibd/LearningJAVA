package openclassroom.Niv1.chap7exceptions;

public class NomVilleException extends Exception {
    public NomVilleException(String message){
        super(message);
    }
}