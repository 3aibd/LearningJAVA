package openclassroom.Niv1.chap7exceptions;


public class Sdz1 {


    public static void main(String[] args) {
        {
            Ville v = null;
            try {
                v = new Ville("Rennes", -12000, "France");
            }

            /*
            //Gestion de l'chap7exceptions sur le nom de la ville
            catch (NombreHabitantException e) {
            }

            //Gestion de l'chap7exceptions sur le nom de la ville
            catch(NomVilleException e2){
                System.out.println(e2.getMessage());
            }
            */

            //Reunion des deux catch
            //Gestion de plusieurs exceptions différentes
            catch (NombreHabitantException | NomVilleException e2){
                System.out.println(e2.getMessage());
            }

            finally {
                if (v == null)
                    v = new Ville();
            }
            System.out.println(v.toString());
        }
    }
}

