package openclassroom.Niv1.chap13newJava8Lambdas.lambdas;

public class Main {

    public static void main(String[] args) {

        //avec les classes anonymes
        Dialoguer d = new Dialoguer() {
            public void parler(String question) {
                System.out.println("Tu as dis : " + question);
            }
        };
        d.parler("Bonjour");

        //Avec la lambdas
        Dialoguer d2 = (s) -> System.out.println("Tu as dis : " + s);
        d2.parler("Bonjour");

    }
}
