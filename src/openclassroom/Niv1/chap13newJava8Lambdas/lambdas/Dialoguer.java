package openclassroom.Niv1.chap13newJava8Lambdas.lambdas;
@FunctionalInterface
public interface Dialoguer {
    public void parler(String question);
}
