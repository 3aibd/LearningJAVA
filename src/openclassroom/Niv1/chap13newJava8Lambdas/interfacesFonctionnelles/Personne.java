package openclassroom.Niv1.chap13newJava8Lambdas.interfacesFonctionnelles;

public class Personne {

    int Age;
    String Nom;

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        this.Age = age;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        this.Nom = nom;
    }

    public String toString(){
        return "#Nom : "+ this.getNom() +" - âge : "+ this.getAge() + "#";
    }

    Personne(){

    }

    Personne(int age,String nom){
        this.Age = age;
        this.Nom = nom;
    }
}
