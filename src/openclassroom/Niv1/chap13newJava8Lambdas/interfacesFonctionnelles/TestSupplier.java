package openclassroom.Niv1.chap13newJava8Lambdas.interfacesFonctionnelles;

import java.util.function.Supplier;

public class TestSupplier {

        public static void main(String[] args) {

            //avec : java.util.function.Supplier<T>
            Supplier<String> s1 = () -> new String("hello !");
            System.out.println(s1.get());
            Supplier<Personne> s2 = () -> new Personne(50, "Dédé");
            System.out.println(s2.get());
        }
}
