package openclassroom.Niv1.chap13newJava8Lambdas.classesAnonymes;

public class Test {
    public static void main(String[] args) {

    }
}
/*
Les classes anonymes sont soumises aux mêmes règles que les classes « normales » :

utilisation des méthodes non redéfinies de la classe mère ;

obligation de redéfinir toutes les méthodes d'une interface ;

obligation de redéfinir les méthodes abstraites d'une classe abstraite.

Cependant, ces classes possèdent des restrictions à cause de leur rôle et de leur raison d'être :

elles ne peuvent pas être déclarées  abstract  ;

elles ne peuvent pas non plus être déclarées  static  ;

elles ne peuvent pas définir de constructeur ;

elles sont automatiquement déclarées  final  : on ne peut dériver de cette classe, l'héritage est donc impossible !

Il y a moyen d'écrire encore moins de code avec les lambdas mais, avant, je dois vous parler du pillier des lambdas : les interfaces fonctionnelles


 */