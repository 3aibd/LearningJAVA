package openclassroom.Niv1.chap10genericite;
import java.util.ArrayList;
import java.util.List;

public class Test {

    Duo duo;

    //Constructeur
    public Test(Duo duo) {
        this.duo = duo;
    }

    public static void main(String[] args) {

        //Exemple 1
        //Solo<Integer> val = new Solo<Integer>(12);
        //int nbre = val.getValeur();


        //Exemple 2
        //Solo<Integer> val = new Solo<Integer>();
        //Solo<String> valS = new Solo<String>("TOTOTOTO");
        //Solo<Float> valF = new Solo<Float>(12.2f);
        // Solo<Double> valD = new Solo<Double>(12.202568);

        //exemple 3
        /*
        int i = new Integer(12);         //Est équivalent à int i = 12
        double d = new Double(12.2586);  //Est équivalent à double d = 12.2586
        Double d = 12.0;
        Character c = 'C';
        al = new ArrayList();
        //Avant Java 5 il fallait faire al.add(new Integer(12))
        //Depuis Java 5 il suffit de faire
        al.add(12);
        */

        //Test Duo
        //Duo<String, Boolean> dual = new Duo<String, Boolean>("toto", true);
        //System.out.println("Valeur de l'objet dual : val1 = " + dual.getValeur1() + ", val2 = " + dual.getValeur2());

        //Duo<Double, Character> dual2 = new Duo<Double, Character>(12.2585, 'C');
        //System.out.println("Valeur de l'objet dual2 : val1 = " + dual2.getValeur1() + ", val2 = " + dual2.getValeur2());


        //généricite avec les chap9collections (ne pas oublier le constructeur)
        /*
        System.out.println("Liste de String");
        System.out.println("------------------------------");
        List<String> listeString= new ArrayList<String>();
        listeString.add("Une chaîne");
        listeString.add("Une autre");
        listeString.add("Encore une autre");
        listeString.add("Allez, une dernière");

        for(String str : listeString)
            System.out.println(str);

        System.out.println("\nListe de float");
        System.out.println("------------------------------");

        List<Float> listeFloat = new ArrayList<Float>();
        listeFloat.add(12.25f);
        listeFloat.add(15.25f);
        listeFloat.add(2.25f);
        listeFloat.add(128764.25f);

        for(float f : listeFloat)
            System.out.println(f);
        */

        //Exemple interdit
        /*
        List<Voiture> listVoiture = new ArrayList<Voiture>();
        List<VoitureSansPermis> listVoitureSP = new ArrayList<VoitureSansPermis>();

        listVoiture = listVoitureSP;   //Interdit !
        */

        //Solution : Utilisation du wildcard
        //Liste de voiture
        /*
        List<Voiture> listVoiture = new ArrayList<Voiture>();
        listVoiture.add(new Voiture());
        listVoiture.add(new Voiture());

        List<VoitureSansPermis> listVoitureSP = new ArrayList<VoitureSansPermis>();
        listVoitureSP.add(new VoitureSansPermis());
        listVoitureSP.add(new VoitureSansPermis());

        affiche(listVoiture);
        affiche(listVoitureSP);
        */

        //Solution 2
        //List n'acceptant que des instances de Voiture ou de ses sous-classes
        //Liste de voiture

        List<Voiture> listVoiture = new ArrayList<Voiture>();
        listVoiture.add(new Voiture());
        listVoiture.add(new Voiture());

        List<Object> listVoitureSP = new ArrayList<Object>();
        listVoitureSP.add(new Object());
        listVoitureSP.add(new Object());

        affiche(listVoiture);

    }

    //Avec cette méthode, on accepte aussi bien les chap9collections de Voiture que les collection de VoitureSansPermis
    /*
    static void affiche(List<? extends Voiture> list){

        for(Voiture v : list)
            System.out.print(v.toString());
    }
    */

    //Avec cette méthode, on accepte aussi bien les chap9collections de Voiture que les chap9collections d'Object : superclasse de toutes les classes

    static void affiche(List<? super Voiture> list){
        for(Object v : list)
            System.out.print(v.toString());
    }
}
/*

La généricité est un concept très utile pour développer des objets travaillant avec plusieurs types de données.

Vous passerez donc moins de temps à développer des classes traitant de façon identique des données différentes.

La généricité permet de réutiliser sans risque le polymorphisme avec les chap9collections.

Cela confère plus de robustesse à votre code.

Vous pouvez coupler les chap9collections avec la généricité !

Le wildcard (?) permet d'indiquer que n'importe quel type peut être traité et donc accepté !

Dès que le wildcard (?) est utilisé, cela revient à rendre ladite collection en lecture seule !

Vous pouvez élargir le champ d'acceptation d'une collection générique grâce au mot-clé extends.

L'instruction ? extends MaClasse autorise toutes les chap9collections de classes ayant pour supertype MaClasse.

L'instruction ? super MaClasse autorise toutes les chap9collections de classes ayant pour type MaClasse et tous ses supertypes !

Pour ce genre de cas, les méthodes génériques sont particulièrement adaptées et permettent d'utiliser le polymorphisme dans toute sa splendeur !
 */


