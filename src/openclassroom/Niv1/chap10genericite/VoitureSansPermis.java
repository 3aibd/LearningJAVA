package openclassroom.Niv1.chap10genericite;

public class VoitureSansPermis <T> extends Voiture {

    //Variable d'instance
    private T valeur;

    //Constructeur par défaut
    public VoitureSansPermis(){
        this.valeur = null;
    }

    //Constructeur avec paramètre inconnu pour l'instant
    public VoitureSansPermis(T val){
        this.valeur = val;
    }

    /*
    //Définit la valeur avec le paramètre
    public void setValeur(T val){
        this.valeur = val;
    }
    */


    //Retourne la valeur déjà « castée » par la signature de la méthode !
    public T getValeur(){
        return this.valeur;
    }
}
