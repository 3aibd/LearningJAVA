package openclassroom.Niv1.chap10genericite;
import java.util.ArrayList;
import java.util.List;


public class Garage {
    List<Voiture> list = new ArrayList<Voiture>();

    public void add(List<? extends Voiture> listVoiture){
        for(Voiture v : listVoiture)
            list.add(v);

        System.out.println("Contenu de notre garage :");
        for(Voiture v : list)
            System.out.print(v.toString());
    }
    public static void main(String[] args){
        List<Voiture> listVoiture = new ArrayList<Voiture>();
        listVoiture.add(new Voiture());

        List<VoitureSansPermis> listVoitureSP = new ArrayList<VoitureSansPermis>();
        listVoitureSP.add(new VoitureSansPermis());

        Garage garage = new Garage();
        garage.add(listVoiture);
        System.out.println("--------------------------");
        garage.add(listVoitureSP);
    }
}

