package openclassroom.Niv1.chap14newJava8LesStreams;

//Et une classe de test :
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//**********************        enumerator couleur      **********************************

/**
 *
 */
enum Couleur {
    MARRON("marron"),
    BLEU("bleu"),
    VERT("vert"),
    VERRON("verron"),
    INCONNU("non déterminé"),
    ROUGE("rouge mais j'avais piscine...");

    private String name = "";

    Couleur(String n){name = n;}
    public String toString() {return name;}
}

//************************      Classe Personne     *************************************
public class Personne {

    public Double taille = 0.0d, poids = 0.0d;
    public String nom = "", prenom = "";
    public Couleur yeux = Couleur.INCONNU;

    public Personne() {	}

    /**
     Constructeur Personne
     @param taille
     @param poids
     @param nom
     @param prenom
     @param yeux
     */
    public Personne(double taille, double poids, String nom, String prenom, Couleur yeux) {
        super();
        this.taille = taille;
        this.poids = poids;
        this.nom = nom;
        this.prenom = prenom;
        this.yeux = yeux;
    }

    /**
     Redéfinition de la fonction toString
     * @return
     */
    public String toString() {
        String s = "Je m'appelle " + nom + " " + prenom;
        //Ajout des autres attribus dans le String
        s += ", je pèse " + poids + " Kg";
        s += ", et je mesure " + taille + " cm.";
        return s;
    }
    public Double getTaille() {return taille;}
    public void setTaille(Double taille) {this.taille = taille;}
    public Double getPoids() {return poids;}
    public void setPoids(Double poids) {this.poids = poids;}
    public String getNom() {return nom;}
    public void setNom(String nom) {this.nom = nom;}
    public String getPrenom() {return prenom;}
    public void setPrenom(String prenom) {this.prenom = prenom;}
    public Couleur getYeux() {return yeux;}
    public void setYeux(Couleur yeux) {this.yeux = yeux;}
}

//****************************      classe Main       ************************
class TestStream {
    public static void main(String[] args) {
        List<Personne> listP = Arrays.asList(
                new Personne(1.80, 70, "A", "Nicolas", Couleur.BLEU),
                new Personne(1.56, 50, "B", "Nicole", Couleur.VERRON),
                new Personne(1.75, 65, "C", "Germain", Couleur.VERT),
                new Personne(1.68, 50, "D", "Michel", Couleur.ROUGE),
                new Personne(1.96, 65, "E", "Cyrille", Couleur.BLEU),
                new Personne(2.10, 120, "F", "Denis", Couleur.ROUGE),
                new Personne(1.90, 90, "G", "Olivier", Couleur.VERRON)
        );


        //Utilisation de forEach  de notre objet Stream : Consumer< ? Super T>  en paramètre
        Stream<Personne> sp = listP.stream();
        sp.forEach(System.out::println);

        //Autre methode via Stream infini par un foreach
        //affichage de son contenu avec une référence de méthode
        //Stream.iterate(1, (x) -> x + 1).forEach(System.out::println);


        //Le premier paramètre de la méthode  iterate()  est le point de départ de l'itération et le second l'opération faite sur le premier paramètre.
        // On adosse la méthode de parcours à cela et nous avons un stream infini. Il est toutefois possible de limiter le nombre d'iteration avec la méthode  limit()  ,
        // comme ceci :


        //Stream.iterate(2d, (x) -> x + 1).limit(100).forEach(System.out::println);

        //---------------------------------------------------
        //Opération : FILTRAGE
        //Nous prenions uniquement les personnes ayant >50 kg
        //---------------------------------------------------

        System.out.println("\nAprès le filtre");
        //Création d'un nouveau Stream car le stream auparavant est detruit.
        sp = listP.stream();

        sp.	filter(x -> x.getPoids() > 50)
                .forEach(System.out::println);

        //---------------------------------------------------
        //Opération : MAP
        //conserve que le poids des personnes
        //---------------------------------------------------
        System.out.println("\nAprès le filtre et le map");
        //Création d'un nouveau Stream car le stream auparavant est detruit.
        sp = listP.stream();

        sp.	filter(x -> x.getPoids() > 50)
                .map(x -> x.getPoids())
                .forEach(System.out::println);

        //---------------------------------------------------
        //Opération : REDUCE
        //Calcul de la somme des poids
        //---------------------------------------------------

        /*
        //Méthode numéro 1 :
        //  Risque :
        System.out.println("\nAprès le filtre et le map et reduce");
        //Création d'un nouveau Stream car le stream auparavant est detruit.
        sp = listP.stream();

        Double sum = sp	.filter(x -> x.getPoids() > 50)
                .map(x -> x.getPoids())
                .reduce(0.0d, (x,y) -> x+y);
        System.out.println(sum);

        //Calcul de la sommes des poids
        */

        //Méthode numéro 2 :
        //  Risque :
        System.out.println("\nAprès le filtre et le map et reduce et la somme");
        sp = listP.stream();

        Optional<Double> sum = sp   .filter(x -> x.getPoids() > 50)
                                    .map(x -> x.getPoids())
                                    .reduce((x,y) -> x+y);
        //Permet de gérer le cas d'erreur en renvoyant 0.0 si isPresent() == false
        System.out.println(sum.orElse(0.0));

        //---------------------------------------------------
        //Opération : Count
        //compte le nombre d'éléments restant après les opérations précédentes.
        //---------------------------------------------------
        sp = listP.stream();

        long count = sp     .filter(x -> x.getPoids() > 50)
                            .map(x -> x.getPoids())
                            .count();

        System.out.println("\nNombre d'éléments : " + count);

        //---------------------------------------------------
        //Opération : Collect
        //Permet de récupérer le résultat des opérations successives sous une certaines forme
        //---------------------------------------------------
        sp = listP.stream();

        //Utilisation de la lambda
        List<Double> ld = sp    .filter(x -> x.getPoids() > 50)
                                .map(x -> x.getPoids())
                                .collect(Collectors.toList());
        System.out.println("\ncontenu de la collection:");
        System.out.println(ld);


        //---------------------------------------------------
    }
}

/*
Autre opérations :

peek  : opération intermédiaire perrmettant de « déboguer » entre chaque opération.

limit  ou  skip  : opération également intermédiaire permettant de limiter ou bloquer des éléments d'un stream.

findFirst  ,  findAny  : opérations terminales qui permet de trouver des éléments particuliers d'un stream.
 */