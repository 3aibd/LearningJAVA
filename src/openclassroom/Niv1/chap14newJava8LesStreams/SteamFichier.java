package openclassroom.Niv1.chap14newJava8LesStreams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class motDico{
        public String mot ="";
        public String alphaMot = "";


        public motDico(){ }

        public motDico(String mot,String alphaMot){
            super();
            this.mot = mot;
            this.alphaMot = mot;
        }

        public String toString(){
            String s = "mot trouvé : " + mot ;
            s += "Groupe dico : " + alphaMot;
            return s;
        }

        //GETTER & SETTER


    public String getMot() {
        return mot;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    public String getAlphaMot() {
        return alphaMot;
    }

    public void setAlphaMot(String alphaMot) {
        this.alphaMot = alphaMot;
    }
}


public class SteamFichier {
    public static void main(String[] args) {
        String fileName = "dictionnaire2";
        try(Stream<String> sf = Files.lines(Paths.get(fileName))){
            sf.forEach(System.out::println);
        }catch(IOException e) {
            e.printStackTrace();
        }

        //Faire une recherche d'un mot et ressortir ce mot;
        //Utiliser la fontion Filtrage, Map et Reduce

    }
}
