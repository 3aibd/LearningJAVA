package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.animauxClasseAbstraites;

public class Tigre extends Felin {

    public Tigre(){

    }
    public Tigre(String couleur, int poids){
        this.couleur = couleur;
        this.poids = poids;
    }

    protected void crier() {
        System.out.println("Je grogne très fort !");
    }
}
