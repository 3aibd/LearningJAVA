package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.animauxClasseAbstraites;

public class Lion extends Felin {

    public Lion(){

    }

    public Lion(String couleur, int poids){
        this.couleur = couleur;
        this.poids = poids;
    }

    protected void crier() {
        System.out.println("Je rugis dans la savane !");
    }
}
