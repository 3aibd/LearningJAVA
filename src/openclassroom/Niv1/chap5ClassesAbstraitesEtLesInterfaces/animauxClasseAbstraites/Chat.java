package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.animauxClasseAbstraites;

public class Chat extends Felin {

    public Chat(){

    }
    public Chat(String couleur, int poids){
        this.couleur = couleur;
        this.poids = poids;
    }

    protected void crier() {
        System.out.println("Je miaule sur les toits !");
    }
}
