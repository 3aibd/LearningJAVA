package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.animauxClasseAbstraites;

public class Chien extends Canin {

    public Chien(){

    }

    public Chien(String couleur, int poids){
        this.couleur = couleur;
        this.poids = poids;
    }

    protected void crier() {
        System.out.println("J'aboie sans raison !");
    }
}
