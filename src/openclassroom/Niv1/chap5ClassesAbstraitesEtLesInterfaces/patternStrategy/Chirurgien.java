package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy;


import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.*;
import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement.*;

public class Chirurgien extends Personnage {

    public Chirurgien(){
        this.soin = new Operation();
    }
    //Constructor
    public Chirurgien(EspritCombatif esprit, Soin soin, Deplacement dep) {
        super(esprit, soin, dep);
    }



    /*public void soigner(){
        if(this.sacDeSoin.equals("gros sac"))
            System.out.println("Je fais des merveilles.");
        else
            System.out.println("Je fais des opérations.");
    }
    */

}