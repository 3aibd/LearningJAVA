package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy;

import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.*;
import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement.*;

public class Sniper extends Personnage{

    public Sniper(){
        this.espritCombatif = new CombatArmeDistance();
    }
    public Sniper(EspritCombatif esprit, Soin soin, Deplacement dep) {
        super(esprit, soin, dep);
    }

    /*public void combattre() {
        if(this.armes.equals("fusil à pompe"))
            System.out.println("Attaque au fusil à pompe !");
        else
            System.out.println("Je me sers de mon fusil à lunette !");
    }
    */
}