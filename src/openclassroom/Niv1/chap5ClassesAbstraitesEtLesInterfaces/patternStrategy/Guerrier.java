package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy;

import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.*;
import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement.*;

public class Guerrier extends Personnage {

    //Constructor
    public Guerrier(){
        this.espritCombatif = new CombatPistolet();
    }
    public Guerrier(EspritCombatif esprit, Soin soin, Deplacement dep) {
        super(esprit, soin, dep);
    }



    /*public void combattre() {
        if(this.armes.equals("pistolet"))
            System.out.println("Attaque au pistolet !");
        else if(this.armes.equals("fusil de sniper"))
            System.out.println("Attaque au fusil de sniper !");
        else
            System.out.println("Attaque au couteau !");
    }*/
}