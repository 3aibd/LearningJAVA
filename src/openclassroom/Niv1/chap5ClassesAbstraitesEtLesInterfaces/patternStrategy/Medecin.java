package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy;


import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.*;
import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement.*;

public class Medecin extends Personnage{

    public Medecin() {
        this.soin = new PremierSoin();
    }
    public Medecin(EspritCombatif esprit, Soin soin, Deplacement dep) {
        super(esprit, soin, dep);
    }
    /*public void combattre() {
        if(this.armes.equals("pistolet"))
            System.out.println("Attaque au pistolet !");
        else
            System.out.println("Vive le scalpel !");
    }

    public void soigner(){
        if(this.sacDeSoin.equals("petit sac"))
            System.out.println("Je peux recoudre des blessures.");
        else
            System.out.println("Je soigne les blessures.");
    }*/
}

