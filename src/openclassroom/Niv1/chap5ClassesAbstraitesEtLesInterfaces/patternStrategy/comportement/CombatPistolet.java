package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement;

import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.*;

public class CombatPistolet implements EspritCombatif {

    public void combat() {
        System.out.println("Je combats au pitolet !");
    }
}
