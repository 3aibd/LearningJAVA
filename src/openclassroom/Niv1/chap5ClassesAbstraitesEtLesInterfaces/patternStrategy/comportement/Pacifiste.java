package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement;

import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.*;

public class Pacifiste implements EspritCombatif {
    public void combat() {
        System.out.println("Je ne combats pas !");
    }
}
