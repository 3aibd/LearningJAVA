package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement;

import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.*;

public class Courir implements Deplacement {
    public void deplacer() {
        System.out.println("Je me déplace en courant.");
    }
}