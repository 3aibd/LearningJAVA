package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement;

import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.*;

public class Operation implements Soin {
    public void soigne() {
        System.out.println("Je pratique des opérations !");
    }
}
