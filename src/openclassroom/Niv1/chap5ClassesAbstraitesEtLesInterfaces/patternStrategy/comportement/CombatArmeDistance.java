package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement;

import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.EspritCombatif;

public class CombatArmeDistance implements EspritCombatif {
    public void combat() {
        System.out.println("Je me bats au fusil longue porté !");
    }
}
