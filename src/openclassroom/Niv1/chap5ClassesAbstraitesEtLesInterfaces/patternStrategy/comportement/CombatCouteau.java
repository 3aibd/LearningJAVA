package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.comportement;

import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.patternStrategy.action.*;

public class CombatCouteau implements EspritCombatif {
    public void combat() {
        System.out.println("Je me bats au couteau !");
    }
}