package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.lesInterfaces;

import openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.animauxClasseAbstraites.Canin;

public class Chien2 extends Canin implements Rintintin {

    public Chien2(){

    }
    public Chien2(String couleur, int poids){
        this.couleur = couleur;
        this.poids = poids;
    }

    protected void crier() {
        System.out.println("J'aboie sans raison !");
    }

    public void faireCalin() {
        System.out.println("Je te fais un GROS CÂLIN");
    }

    public void faireLeBeau() {
        System.out.println("Je fais le beau !");
    }

    public void faireLechouille() {
        System.out.println("Je fais de grosses léchouilles...");
    }
}
