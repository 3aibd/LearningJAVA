package openclassroom.Niv1.chap5ClassesAbstraitesEtLesInterfaces.lesInterfaces;

public class Test {
    public static void main(String[] args) {

        //Les méthodes d'un chien
        Chien2 c = new Chien2("Gris bleuté", 20);
        c.boire();
        c.manger();
        c.deplacement();
        c.crier();
        System.out.println(c.toString());

        System.out.println("--------------------------------------------");
        //Les méthodes de l'interface
        c.faireCalin();
        c.faireLeBeau();
        c.faireLechouille();

        System.out.println("--------------------------------------------");
        //Utilisons le polymorphisme de notre interface
        Rintintin r = new Chien2();
        r.faireLeBeau();
        r.faireCalin();
        r.faireLechouille();
    }
}
