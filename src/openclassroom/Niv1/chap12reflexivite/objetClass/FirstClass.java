package openclassroom.Niv1.chap12reflexivite.objetClass;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FirstClass {
    public static void main(String[] args) {

        //-------Connaître la superclasse d'une classe

        Class c = String.class;
        Class c2 = new String().getClass();
        //La fameuse méthode finale dont je vous parlais dans le chapitre sur l'héritage
        //Cette méthode vient de la classe Object
        System.out.println();
        System.out.println("La superclasse de la classe " + String.class.getName() + " est : " + String.class.getSuperclass());

        //--------Connaître la liste des interfaces d'une classe

        //La méthode getInterfaces retourne un tableau de Class
        Class[] faces = c.getInterfaces();
        //Pour voir le nombre d'interfaces
        System.out.println();
        System.out.println("Il y a " + faces.length + " interfaces implémentées");
        //On parcourt le tableau d'interfaces
        for(int i = 0; i < faces.length; i++)
            System.out.println(faces[i]);


        //---------Connaître la liste des méthodes de la classe
        Method[] m = c.getMethods();
        System.out.println();
        System.out.println("Il y a " + m.length + " méthodes dans cette classe");
        //On parcourt le tableau de méthodes
        for(int i = 0; i < m.length; i++)
        {
            System.out.println(m[i]);

            Class[] p = m[i].getParameterTypes();
            for(int j = 0; j < p.length; j++)
                System.out.println(p[j].getName());

            System.out.println("----------------------------------\n");
        }


        //----------Connaître la liste des champs (variable de classe ou d'instance)
        Field[] n = c.getDeclaredFields();

        System.out.println();
        System.out.println("Il y a " + n.length + " champs dans cette classe");
        //On parcourt le tableau de méthodes
        for(int i = 0; i < n.length; i++)
            System.out.println(n[i].getName());

        //-----------Connaître la liste des constructeurs de la classe
        Constructor[] construc = c.getConstructors();
        System.out.println();
        System.out.println("Il y a " + construc.length + " constructeurs dans cette classe");
        //On parcourt le tableau des constructeurs
        for(int i = 0; i < construc.length; i++){
            System.out.println(construc[i].getName());

            Class[] param = construc[i].getParameterTypes();
            for(int j = 0; j < param.length; j++)
                System.out.println(param[j]);

            System.out.println("-----------------------------\n");
        }
    }
}
