package openclassroom.Niv1.chap2heritage;

import java.util.Objects;

public class Ville {

    //Stocke le nom de notre ville
    protected String nomVille;
    //Stocke le nom du pays de notre ville
    protected String nomPays;
    //Stocke le nombre d'habitants de notre ville
    protected int nbreHabitants;
    //Stock la cétegorie de la ville en fonction de nbhabitant
    protected char categorie;

    //Variables publiques qui comptent les instances
    public static int nbreInstances = 0;

    //Variable privée qui comptera aussi les instances
    private static int nbreInstancesBis = 0;


    //Constructeur par défaut
    public Ville(){
        System.out.println("Création d'une ville !");
        //On incrémente nos variables à chaque appel aux constructeurs
        nbreInstances++;
        nbreInstancesBis++;

        nomVille = "Inconnu";
        nomPays = "Inconnu";
        nbreHabitants = 0;
    }

    //Constructeur avec paramètres
    //J'ai ajouté un « p » en première lettre des paramètres.
    //Ce n'est pas une convention, mais ça peut être un bon moyen de les repérer.
    public Ville(String pNom, int pNbre, String pPays)
    {
        System.out.println("Création d'une ville avec des paramètres !");
        //On incrémente nos variables à chaque appel aux constructeurs
        nbreInstances++;
        nbreInstancesBis++;

        nomVille = pNom;
        nomPays = pPays;
        nbreHabitants = pNbre;
        this.setCategorie();
    }

    //la classe Objects
    public String toString(){
        return "\t"+this.nomVille+" est une ville de "+this.nomPays+", elle comporte : "+this.nbreHabitants+" => elle est donc de catégorie : "+this.categorie;
    }

    //Redéfinition des méthodes via la classe Objects
    public int hashCode() {
        return Objects.hash(categorie, nbreHabitants, nomPays, nomVille);
    }

    public boolean equals(Object obj) {
        //On vérifie si les références d'objets sont identiques
        if (this == obj)
            return true;

        //On s'assure que les objets sont du même type, ici de type Ville
        if (getClass() != obj.getClass())
            return false;

        //Maintenant, on compare les attributs de nos objets
        Ville other = (Ville) obj;

        return Objects.equals(other.getCategorie(), this.getCategorie()) &&
                Objects.equals(other.getNom(), this.getNom()) &&
                Objects.equals(other.getNombreHabitants(), this.getNombreHabitants()) &&
                Objects.equals(other.getNomPays(), this.getNomPays());
    }


    //Retourne la description de la ville
    public String decrisToi(){
        return "\t"+this.nomVille+" est une ville de "+this.nomPays+ ", elle comporte : "+this.nbreHabitants+" habitant(s) => elle est donc de catégorie : "+this.categorie;
    }

    //Retourne une chaîne de caractères selon le résultat de la comparaison
    public String comparer(Ville v1){
        String str = new String();

        if (v1.getNombreHabitants() > this.nbreHabitants)
            str = v1.getNom()+" est une ville plus peuplée que "+this.nomVille;

        else
            str = this.nomVille+" est une ville plus peuplée que "+v1.getNom();

        return str;
    }



    //*************   ACCESSEURS *************

    //Retourne le nom de la ville
    public String getNom()  {
        return nomVille;
    }

    //Retourne le nom du pays
    public String getNomPays()
    {
        return nomPays;
    }

    // Retourne le nombre d'habitants
    public int getNombreHabitants()
    {
        return nbreHabitants;
    }

    public char getCategorie(){
        return categorie;
    }

    public static int getNombreInstancesBis()
    {
        return nbreInstancesBis;
    }

    //*************   MUTATEURS   *************

    //Définit le nom de la ville
    public void setNom(String pNom)
    {
        nomVille = pNom;
    }

    //Définit le nom du pays
    public void setNomPays(String pPays)
    {
        nomPays = pPays;
    }

    //Définit le nombre d'habitants
    public void setNombreHabitants(int nbre)
    {
        nbreHabitants = nbre;
    }

    //Définit la catégorie de la ville
    private void setCategorie() {

        int bornesSuperieures[] = {0, 1000, 10000, 100000, 500000, 1000000, 5000000, 10000000};
        char categories[] = {'?', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};

        int i = 0;
        while (i < bornesSuperieures.length && this.nbreHabitants > bornesSuperieures[i])
            i++;

        this.categorie = categories[i];
    }


    //*******************************************************       MAIN       ************************************************
    public static void main(String[] args) {
        Ville v = new Ville();
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances);
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombreInstancesBis());

        Ville v1 = new Ville("Marseille", 1236, "France");
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances);
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombreInstancesBis());


        Ville v2 = new Ville("Rio", 321654, "Brésil");
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances);
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombreInstancesBis());

        System.out.println("\n\n"+v1.decrisToi());
        System.out.println(v.decrisToi());
        System.out.println(v2.decrisToi()+"\n\n");
        System.out.println(v1.comparer(v2));

        //System.out.println(v.toString());
        //Est équivalent à
        System.out.println(v);

    }
}
