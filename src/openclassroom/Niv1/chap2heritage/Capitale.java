package openclassroom.Niv1.chap2heritage;

public class Capitale extends Ville {


    private String monument;

    //Constructeur par défaut
    public Capitale() {
        //Ce mot clé appelle le constructeur de la classe mère
        super();
        monument = "aucun";
    }
    //Constructeur d'initialisation de capitale
    public Capitale(String nom, int hab, String pays, String monument){
        super(nom, hab, pays);
        this.monument = monument;
    }

    /**
     * Description d'une capitale
     * @return String retourne la description de l'objet
     */
    public String decrisToi(){
        String str = super.decrisToi() + "\n \t ==>>" + this.monument + "en est un monument";

        return str;
    }

    public String toString(){
        String str = super.toString() + "\n \t ==>>" + this.monument + " en est un monument";
        return str;
    }



    /**
     * @return le nom du monument
     */
    //*************   ACCESSEURS *************
    public String getMonument() {
        return monument;
    }

    //*************   MUTATEURS   *************
    //Définit le nom du monument
    public void setMonument(String monument) {
        this.monument = monument;
    }


    //*******************************************************       MAIN       ************************************************
    public static void main(String[] args) {

        //PART 1
        /*Capitale cap = new Capitale("Paris", 654987, "France", "la tour Eiffel");
        System.out.println("\n" + cap.decrisToi());


        //Définition d'un tableau de villes null
        Ville[] tableau = new Ville[6];

        //Définition d'un tableau de noms de villes et un autre de nombres d'habitants
        String[] tab = {"Marseille", "lille", "caen", "lyon", "paris", "nantes"};
        int[] tab2 = {123456, 78456, 654987, 75832165, 1594, 213};

        //Les trois premiers éléments du tableau seront des villes,
        //et le reste, des capitales
        for(int i = 0; i < 6; i++){
            if (i <3){
                Ville V = new Ville(tab[i], tab2[i], "france");
                tableau[i] = V;
            }

            else{
                Capitale C = new Capitale(tab[i], tab2[i], "france", "la tour Eiffel");
                tableau[i] = C;
            }
        }

        //Il ne nous reste plus qu'à décrire tout notre tableau !
        for(Ville V : tableau){
            System.out.println(V.decrisToi()+"\n");
        }
        */

        //PART 2 AVEC POLYMORPHISME
        //Définition d'un tableau de villes null
        Ville[] tableau = new Ville[6];

        //Définition d'un tableau de noms de Villes et un autre de nombres d'habitants
        String[] tab = {"Marseille", "lille", "caen", "lyon", "paris", "nantes"};
        int[] tab2 = {123456, 78456, 654987, 75832165, 1594, 213};

        //Les trois premiers éléments du tableau seront des Villes
        //et le reste des capitales
        for(int i = 0; i < 6; i++){
            if (i <3){
                Ville V = new Ville(tab[i], tab2[i], "france");
                tableau[i] = V;
            }

            else{
                Capitale C = new Capitale(tab[i], tab2[i], "france", "la tour Eiffel");
                tableau[i] = C;
            }
        }

        //Il ne nous reste plus qu'à décrire tout notre tableau !
        for(Object obj : tableau){
            System.out.println(obj.toString()+"\n");
        }
    }

}

