package openclassroom.Niv1.chap2heritage;

public class Sdz1 {


    public static void main(String[] args) {
        Ville[] tableau = new Ville[6];
        String[] tab = {"Marseille", "lille", "caen", "lyon", "paris", "nantes"};
        int[] tab2 = {123456, 78456, 654987, 75832165, 1594, 213};

        for(int i = 0; i < 6; i++){
            if (i <3){
                Ville V = new Ville(tab[i], tab2[i], "france");
                tableau[i] = V;
            }

            else{
                Capitale C = new Capitale(tab[i], tab2[i], "france", "la tour Eiffel");
                tableau[i] = C;
            }
        }

        //Il ne nous reste plus qu'à décrire tout notre tableau !
        for(Object v : tableau){

            //transtypage de v en ville
            //Applique la méthodedecrisToi()à la référence appelante, c'est-à-dire, ici, une référenceObjectchangée enVille.
            System.out.println(((Ville)v).decrisToi()+"\n");
        }
    }
}
