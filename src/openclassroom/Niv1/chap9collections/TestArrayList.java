package openclassroom.Niv1.chap9collections;
import java.util.ArrayList;

//LIST
public class TestArrayList {

    public static void main(String[] args) {

        ArrayList al = new ArrayList();
        al.add(12);
        al.add("Une chaîne de caractères !");
        al.add(12.20f);
        al.add('d');

        for(int i = 0; i < al.size(); i++)
        {
            System.out.println("donnée à l'indice " + i + " = " + al.get(i));
        }
    }
}

/*
    Une collection permet de stocker un nombre variable d'objets.

        Il y a principalement trois types de collection : les List, les Set et les Map.

        Chaque type a ses avantages et ses inconvénients.

        Les Collection stockent des objets alors que les Map stockent un couple clé - valeur.

        Si vous insérez fréquemment des données en milieu de liste, utilisez une LinkedList.

        Si vous voulez rechercher ou accéder à une valeur via une clé de recherche, optez pour une collection de type Map.

        Si vous avez une grande quantité de données à traiter, tournez-vous vers une liste de type Set
*/