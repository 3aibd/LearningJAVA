package openclassroom.Niv1.chap9collections;
import java.util.HashSet;
import java.util.Iterator;

//SET
public class TestHashSet {
    public static void main(String[] args) {
        HashSet hs = new HashSet();
        hs.add("toto");
        hs.add(12);
        hs.add('d');
        //parcoure avec un Iterator
        Iterator it = hs.iterator();
        while(it.hasNext())
            System.out.println(it.next());

        //parcoure avec un tableau d'objet
        System.out.println("\nParcours avec un tableau d'objet");
        System.out.println("-----------------------------------");

        Object[] obj = hs.toArray();
        for(Object o : obj)
            System.out.println(o);
    }

}

/*
    Une collection permet de stocker un nombre variable d'objets.

        Il y a principalement trois types de collection : les List, les Set et les Map.

        Chaque type a ses avantages et ses inconvénients.

        Les Collection stockent des objets alors que les Map stockent un couple clé - valeur.

        Si vous insérez fréquemment des données en milieu de liste, utilisez une LinkedList.

        Si vous voulez rechercher ou accéder à une valeur via une clé de recherche, optez pour une collection de type Map.

        Si vous avez une grande quantité de données à traiter, tournez-vous vers une liste de type Set
*/