package openclassroom.Niv1.chap9collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

//LISTE
public class TestLinkedList {
    public static void main(String[] args) {
        List l = new LinkedList();
        l.add(12);
        l.add("toto ! !");
        l.add(12.20f);

        //parcours Sans Iterator
        //for(int i = 0; i < l.size(); i++)
        //    System.out.println("Élément à l'index " + i + " = " + l.get(i));

        //parcours avec un Iterator
        for(int i = 0; i < l.size(); i++)
            System.out.println("Élément à l'index " + i + " = " + l.get(i));

        System.out.println("\n \tParcours avec un itérateur ");
        System.out.println("-----------------------------------");
        ListIterator li = l.listIterator();

        while(li.hasNext())
            System.out.println(li.next());
    }
}

/*
    Une collection permet de stocker un nombre variable d'objets.

        Il y a principalement trois types de collection : les List, les Set et les Map.

        Chaque type a ses avantages et ses inconvénients.

        Les Collection stockent des objets alors que les Map stockent un couple clé - valeur.

        Si vous insérez fréquemment des données en milieu de liste, utilisez une LinkedList.

        Si vous voulez rechercher ou accéder à une valeur via une clé de recherche, optez pour une collection de type Map.

        Si vous avez une grande quantité de données à traiter, tournez-vous vers une liste de type Set
*/