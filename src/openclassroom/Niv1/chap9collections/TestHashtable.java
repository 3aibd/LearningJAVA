package openclassroom.Niv1.chap9collections;
import java.util.Enumeration;
import java.util.Hashtable;

//MAP
public class TestHashtable {

        public static void main(String[] args) {

            Hashtable ht = new Hashtable();
            ht.put(1, "printemps");
            ht.put(10, "été");
            ht.put(12, "automne");
            ht.put(45, "hiver");

            Enumeration e = ht.elements();

            while(e.hasMoreElements()){
                System.out.println(e.nextElement());


            }

        }
}

/*
    Une collection permet de stocker un nombre variable d'objets.

        Il y a principalement trois types de collection : les List, les Set et les Map.

        Chaque type a ses avantages et ses inconvénients.

        Les Collection stockent des objets alors que les Map stockent un couple clé - valeur.

        Si vous insérez fréquemment des données en milieu de liste, utilisez une LinkedList.

        Si vous voulez rechercher ou accéder à une valeur via une clé de recherche, optez pour une collection de type Map.

        Si vous avez une grande quantité de données à traiter, tournez-vous vers une liste de type Set
*/