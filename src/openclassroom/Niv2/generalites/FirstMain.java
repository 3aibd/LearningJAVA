package openclassroom.Niv2.generalites;
import java.util.ArrayList;
import java.util.Iterator;

public class FirstMain {
    public static void main(String[] args) {
        //nous créons une collection basique
        ArrayList<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");

        //Un petit compteur pour récupérer les tours de boucle
        int nbTourDeBoucle = 0;
        int nbTourDeBoucle2 = 0;

        //Nous récupérons notre itérateur
        //Iterator it = list.iterator();

        //Evite le Cast
        Iterator<String> it = list.iterator();

        //tant qu'il y a des éléments à parcourir

        //---------------------------------------------------------------
        // Méthode de parcoure avec Iterateur

        // Premier boucle de parcours
        //explication
        //La méthode hasNext() sera donc automatiquement au courant qu'un élément n'est plus présent...
        // J'ajouterai que la méthode remove() est la seule façon sûre de supprimer un élément d'une collection pendant un parcours.
        /*
        while(it.hasNext()){

            nbTourDeBoucle++;
            //nous récupérons l’élément courant
            String str = (String)it.next();
            //si nous sommes sur l'élément 4, nous le retirons de la collection
            if(str.equals("4"))
                it.remove();
        }
        */
        //nous reparcourons un nouvel itérateur
        //pour nous assurer que tout a fonctionné
        it = list.iterator();

        //Deuxième boucle de parcours
        /*while(it.hasNext()){

            nbTourDeBoucle2++;
            System.out.println(it.next());
            System.out.println(it.next());
            System.out.println(it.next());
        }
        */

        //---------------------------------------------------------------
        // Deuxième méthode de parcours (parcours simple)
        for(String element : list)
            System.out.println(element);

        //Ou encore
        //for(int i = 0; i < list.size(); i++)
        //    System.out.println(list.get(i));


        System.out.println("Nombre de tours de boucle N°1 : " + nbTourDeBoucle);
        System.out.println("Nombre de tours de boucle N°2 : " + nbTourDeBoucle2);

    }
}
