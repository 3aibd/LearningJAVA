package openclassroom.Niv2.generalites;

public interface Iterator<E> {

    // parcourir une collection, via la méthode hasNext().
    // Cette méthode retourne true s'il reste des éléments à parcourir ;
    boolean hasNext();

    //récupérer un élément, grâce à la méthode next(). Celle-ci retourne l'élément courant dans l'itérateur.
    // Si vous invoquez cette méthode plusieurs fois dans une boucle de parcours,
    // cela vous fait avancer dans la lecture de la collection,
    // un peu comme si vous incrémentiez plusieurs fois le compteur d'une boucle for ;
    E next();


    //supprimer un élément, en utilisant la méthode remove().
    // Cette méthode n'est utilisable qu'après avoir utilisé la méthode next()
    // et n'est utilisable qu'une seule fois par appel à la méthode next().
    void remove();
}


//Lorsque vous parcourez une collection, vous ne pouvez supprimer qu'un seul élément par tour de boucle, sinon une exception sera levée !